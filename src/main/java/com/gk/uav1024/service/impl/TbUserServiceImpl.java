package com.gk.uav1024.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gk.uav1024.common.CommonResult;
import com.gk.uav1024.common.ResultCode;
import com.gk.uav1024.entity.TbUser;
import com.gk.uav1024.mapper.TbUserMapper;
import com.gk.uav1024.service.ITbUserService;
import com.gk.uav1024.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author author
 * @since 2023-09-12
 */
@Service
public class TbUserServiceImpl extends ServiceImpl<TbUserMapper, TbUser> implements ITbUserService {

    @Autowired
    private JwtUtil jwtUtils;

//    @Autowired
//    private RedisCache redisCache;

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * 用户登入
     * @param loginDto 账号密码输入
     * @return jwt
     */

    @Autowired
    private PasswordEncoder bcryptPasswordEncoder;

    /**
     * 用户注册
     * 逻辑很简单, 就是将内容传入
     * @param
     * @return
     */

//    public int userRegister(RegisterDto registerDto) {
//        // 先查询用户是否存在, 存在就用ResultInfo返回消息即可
//        nullOrNot.isTrue(usersMapper.queryUserByEmail(registerDto.getEmail()) != null, "用户名已存在");
//
//        //创建用户, 设置其中的内容
//        User user = new User();
//        user.setUserName(registerDto.getUsername());
//        user.setUserEmail(registerDto.getEmail());
//        // 在这里将用户密码进行加密, 存入数据库 (可不能对密码明文存储)
//        user.setUserPassword(bcryptPasswordEncoder.encode(registerDto.getPassword()));
//        user.setUserRole("user");
//        user.setEnabled(1); //经过管理员确认后才可使用 TODO:这里需要修改
//        user.setAccountNoExpired(1);
//        user.setCredentialsNoExpired(1);
//        user.setAccountNoLocked(1);
//        user.setUserToken("a");
//
//        return usersMapper.registerUser(user);
//    }


//    @Override
//    public void userLogout(){
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        SecurityUser loginUser = (SecurityUser) authentication.getPrincipal();
//        int userid = loginUser.getUser().getUserId();
//        redisCache.deleteObject("login:"+userid);
//    }

    @Override
    public CommonResult<TbUser> loadUserByUsername(String username) throws UsernameNotFoundException {
        LambdaQueryWrapper<TbUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TbUser::getUsername,username);
        TbUser tbUser = getOne(queryWrapper);
        if (tbUser == null) {
            throw new UsernameNotFoundException("username not found");
        }
        return null;
//        // 在这里将用户的权限转换为字符串数组
//        List<String> list = new ArrayList<>(Arrays.asList(tbUser.get()));
//        return new SecurityUser(user, list);
    }

    @Override
    public CommonResult<TbUser> login(TbUser user) {
        LambdaQueryWrapper<TbUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TbUser::getUsername,user.getUsername());
        queryWrapper.eq(TbUser::getPwd,user.getPwd());
        // this.getOne()
        TbUser result = getOne(queryWrapper);

        if(ObjectUtil.isNull(result)){
            return CommonResult.failed(ResultCode.DATAULL);
        }

        return CommonResult.success(result,"登录成功");

//        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPwd());
//        // 使用authenticationManager调用loadUserByUsername获取数据库中的用户信息,
//        Authentication authentication = authenticationManager.authenticate(authToken);
//        if(authentication == null) {
//            throw new RuntimeException("Login false");
//        }
//
//        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
//        Long useId = securityUser.getUser().getId();
//        String usrName = securityUser.getUsername();
//
//        List<String> authList = new ArrayList<String>();
//        for (GrantedAuthority auth : securityUser.getAuthorities()) {
//            authList.add(auth.getAuthority());
//        }
//        String jwt = JwtUtil.createJwt("user login", useId);
//        // 存入Redis
//        redisCache.setCacheObject("login:"+useId,securityUser);
//        HashMap<String, String> map = new HashMap<String, String>();
//        map.put("token", jwt);
//        return map;
    }

    @Override
    public CommonResult<TbUser> register(TbUser user) {
        LambdaQueryWrapper<TbUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TbUser::getUsername,user.getUsername());
        TbUser result = getOne(queryWrapper);
        if(ObjectUtil.isNull(result)){
            save(user);
            return CommonResult.success(user,"注册成功");
        }
        return CommonResult.failed("登录失败，该用户名已存在！");
    }

}
