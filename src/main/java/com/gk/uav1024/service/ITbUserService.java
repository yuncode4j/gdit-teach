package com.gk.uav1024.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gk.uav1024.common.CommonResult;
import com.gk.uav1024.entity.TbUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author author
 * @since 2023-09-12
 */
public interface ITbUserService extends IService<TbUser> {
    CommonResult<TbUser> loadUserByUsername(String username);
    CommonResult<TbUser> login(TbUser user);

    CommonResult<TbUser> register(TbUser user);



}
