package com.gk.uav1024.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gk.uav1024.entity.TbUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2023-09-12
 */

@Mapper
public interface TbUserMapper extends BaseMapper<TbUser> {
}
