package com.gk.uav1024.controller;


import cn.hutool.core.util.ObjectUtil;
import com.gk.uav1024.common.CommonResult;
import com.gk.uav1024.common.ResultCode;
import com.gk.uav1024.entity.TbUser;
import com.gk.uav1024.service.ITbUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author author
 * @since 2023-09-12
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户")
public class TbUserController {
    @Resource
    private ITbUserService tbUserService;

    @PostMapping("/login")
    @ApiOperation("登录")
    public CommonResult<TbUser> login(@RequestBody TbUser user) {
        if(ObjectUtil.hasNull(user,user.getUsername()) ){
            return CommonResult.failed(ResultCode.DATAULL);
        }
        return tbUserService.login(user);
    }

    @PostMapping("/register")
    @ApiOperation("注册")
//    @ResponseBody
    public CommonResult<TbUser> register(@RequestBody TbUser user)  {
        if(ObjectUtil.hasNull(user,user.getUsername()) ){
            return CommonResult.failed(ResultCode.DATAULL);
        }
        return tbUserService.register(user);
    }


//    @GetMapping("/logout")
//    @ResponseBody
//    public CommonResult<TbUser> userLogout() {
//        try {
//            tbUserService.userLogout();
//            return new CommonResult().success(2006, "User logout success");
//        }catch (Exception e){
//            e.printStackTrace();
//            return new CommonResult().error(5006, "User logout fail");
//        }
//    }

}
