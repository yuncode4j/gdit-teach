package com.gk.uav1024;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2  //开启swagger2
@MapperScan("com.gk.uav1024.mapper")  // 持久层扫描
public class Uav1024Application {

    public static void main(String[] args) {
        SpringApplication.run(Uav1024Application.class, args);
    }

}
