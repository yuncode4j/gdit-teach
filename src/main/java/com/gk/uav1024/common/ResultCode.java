package com.gk.uav1024.common;

import lombok.Getter;

@Getter
public enum ResultCode implements IErrorCode{
    SUCCESS(200,"操作成功"),
    FAILED(400, "操作失败"),
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    DATAULL(201,"暂无数据");



    private Integer code;
    private String message;
    private ResultCode(int code,String message) {
        this.code = code;
        this.message = message;
    }


}




