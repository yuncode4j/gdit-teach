package com.gk.uav1024.common;

/**
 * 封装API的错误码
 */
public interface IErrorCode {
    Integer getCode();
    String getMessage();
}